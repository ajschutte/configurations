package com.mg.models.configurations;

import java.util.List;

/**
 * @author Andries on 2/1/18.
 */
public interface ConfigurationSourceResolver {

    <T extends Configuration> T loadStandardCompositeConfigurationSources();

    <T extends Configuration> T loadConfigurationSource(ConfigurationSource source);

    ConfigurationSource getLoadedConfigurationSource(String sourceId);

    List<ConfigurationSource> getAllLoadedConfigurationSources();

}
