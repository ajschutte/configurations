package com.mg.models.configurations;

import com.mg.exceptions.core.ResourceConfigurationFailedException;

import java.util.ServiceLoader;

/**
 * @author Andries on 1/25/18.
 */
public interface ConfigurationAgent {

    Object getConfigurableEnvironment();

    <T extends Configuration> T getCompositeConfiguration();

    <T extends Configuration> T getCompositeConfigurationSubset(String prefix);

    <T extends Configuration> T getConfiguration(String sourceId);

    <T extends Configuration> T getConfigurationSubset(String prefix, String sourceId);

    ConfigurationSourceResolver getConfigurationSourceResolver();

    static ConfigurationAgent getConfigurationAgent(String providerId) {

        ServiceLoader<ConfigurationAgentLauncher> loader
                = ServiceLoader.load(ConfigurationAgentLauncher.class);

        ConfigurationAgent agent = null;

        for (ConfigurationAgentLauncher cp : loader) {

            if (cp.getProviderId().equals(providerId)) {
                agent = cp.getConfigurationAgent();
            }

        }

        if (agent == null) {
            throw new ResourceConfigurationFailedException("Could not find configuration provider for ID: " + providerId);
        }

        return agent;

    }

}
