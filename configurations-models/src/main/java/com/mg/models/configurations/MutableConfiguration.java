package com.mg.models.configurations;

/**
 * @author Andries on 1/30/18.
 */
public interface MutableConfiguration extends Configuration {

    <T> void addPropertyOfType(String key, T value, Class<T> type);

    <T> void setPropertyOfType(String key, T value, Class<T> type);

    boolean addSubsetOfProperties(String prefix, Configuration properties);

    boolean removeProperty(String key);

    boolean removeSubsetOfProperties(String prefix);

    boolean removeAllProperties();

}
