package com.mg.models.configurations;

import java.net.URI;
import java.util.List;

/**
 * @author Andries on 2/1/18.
 */
public interface ConfigurationSource {

    enum ConfigurationSourceType { FILESYSTEM, CLASSPATH, CUSTOM_URI, COMPOSITE, COMMAND_LINE, ENVIRONMENT_VARIABLE, CONFIG_SERVER }

    boolean isDynamicallyReloadable();

    boolean registerConfigurationSourceChangeListener(ConfigurationSourceChangeListener listener);

    boolean unregisterConfigurationSourceChangeListener(ConfigurationSourceChangeListener listener);

    String getSourceId();

    List<URI> getSourceUris();

    <T extends Configuration> T loadConfiguration();

    <T extends Configuration> T getConfiguration();

}
