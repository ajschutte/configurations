package com.mg.models.configurations;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Properties;

/**
 * @author Andries on 1/25/18.
 */
public abstract class BaseConfigurationAgent implements ConfigurationAgent {

    private static final Logger logger = LoggerFactory.getLogger(BaseConfigurationAgent.class);

    public static final String CONFIG_SUB_DIR = "/config";

    public static final String CONFIG_BASE_DIR_SYSTEM_PROP = "mg.app.config.basedir";

    public static final String CONFIG_CUSTOM_URL_SYSTEM_PROP = "mg.app.config.url";

    public static final String CONFIG_PROFILES_SYSTEM_PROP = "mg.app.config.profiles";

    protected abstract void initialize();

    protected abstract void shutdown();

    protected File getBaseConfigDir() {

        String home = null;

        Properties p = System.getProperties();

        if (p.containsKey(CONFIG_BASE_DIR_SYSTEM_PROP)) {
            home = p.getProperty(CONFIG_BASE_DIR_SYSTEM_PROP);
        }

        if (StringUtils.isEmpty(home)) {
            home = System.getProperty("user.home");
        }

        if (StringUtils.isNotEmpty(home)) {

            String baseDir = home + CONFIG_SUB_DIR;

            logger.debug("Using config base dir: {}", baseDir);

            return new File(baseDir);
        }
        else {

            logger.warn("Config base dir is null..");

            return null;

        }

    }

}
