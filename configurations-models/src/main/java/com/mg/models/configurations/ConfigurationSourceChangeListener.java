package com.mg.models.configurations;

/**
 * @author Andries on 2/1/18.
 */
public interface ConfigurationSourceChangeListener {

    String getSourceChangeListenerId();

    void configurationSourceChanged(ConfigurationSourceChangedEvent event);

}
