package com.mg.models.configurations;

/**
 * @author Andries on 1/30/18.
 */
public interface ConfigurationDecoder {

    String decode(String s);

}
