package com.mg.models.configurations;

import java.util.List;

/**
 * @author Andries on 1/30/18.
 */
public interface Configuration {

    <T> T getPropertyOfType(String key, Class<T> type);

    <T> T getPropertyOfType(String key, Class<T> type, T defaultVal);

    String getPropertyAsString(String key);

    String getPropertyAsString(String key, String defaultVal);

    <T extends Configuration> T getPropertyAsConfiguration(String key);

    <T extends Configuration> T getPropertiesSubset(String prefix);

    boolean containsKey(String key);

    List<String> getKeysWithPrefix(String prefix);

    List<String> getKeys();

    <T> T getEncodedPropertyOfType(String key, Class<T> type, ConfigurationDecoder decoder);

    String getEncodedPropertyAsString(String key, ConfigurationDecoder decoder);

    Object getConfigurationProvider();

    String getSourceId();

}
