package com.mg.models.configurations;

/**
 * @author Andries on 2/1/18.
 */
public class ConfigurationSourceChangedEvent {

    enum SourceChangedEventType {PROPERTY_MODIFIED, PROPERTY_ADDED, PROPERTY_DELETED, SOURCE_ADDED, SOURCE_REMOVED }

    private Configuration changedConfiguration;

    public Configuration getChangedConfiguration() {
        return changedConfiguration;
    }

}
