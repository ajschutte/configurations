package com.mg.clients.configurations.agent.spring;

import com.mg.models.configurations.BaseConfigurationAgent;
import com.mg.models.configurations.Configuration;
import com.mg.models.configurations.ConfigurationSourceResolver;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.config.ConfigFileApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.ResourceLoader;

import java.util.Arrays;

/**
 * @author Andries on 1/25/18.
 */
public class SpringConfigurationAgent extends BaseConfigurationAgent {

    private static final Logger logger = LoggerFactory.getLogger(SpringConfigurationAgent.class);

    private ConfigurableEnvironment env;

    public SpringConfigurationAgent(ConfigurableEnvironment env, ResourceLoader loader) {

        logger.debug("Invoking c'tor with ConfigurableEnvironment: {}, and ResourceLoader: {}", env, loader);

        this.env = env;

        setConfigBaseDir();

        setConfigActiveProfiles();

        logger.warn("System property for base configuration directory set to: {}",
                env.getProperty(ConfigFileApplicationListener.CONFIG_LOCATION_PROPERTY));

        logger.warn("System property for active configuration profiles set to: {}",
                env.getProperty(ConfigFileApplicationListener.ACTIVE_PROFILES_PROPERTY));

        ConfigFileApplicationListener configResolver = new ConfigFileApplicationListener();
        SpringApplication app = new SpringApplication();
        app.setResourceLoader(loader);
        configResolver.postProcessEnvironment(env, app);

        logger.debug("App config property: {}", env.getProperty("Filesystem.RootPath"));

        logger.debug("Active profiles: {}", Arrays.toString(env.getActiveProfiles()));

    }

    private void setConfigActiveProfiles() {

        String profiles = env.getProperty(CONFIG_PROFILES_SYSTEM_PROP);

        if (StringUtils.isNotEmpty(profiles)) {

            env.getSystemProperties().put(ConfigFileApplicationListener.ACTIVE_PROFILES_PROPERTY, profiles);

        }

    }

    private void setConfigBaseDir() {

        String configBase = env.getProperty(CONFIG_BASE_DIR_SYSTEM_PROP);

        if (StringUtils.isNotEmpty(configBase)) {

            if (!configBase.endsWith("/")) { //spring requires the base dir to end with a /
                configBase = configBase + "/";
            }

            env.getSystemProperties().put(ConfigFileApplicationListener.CONFIG_LOCATION_PROPERTY, configBase);
        }

    }

    @Override
    public Object getConfigurableEnvironment() {
        return env;
    }

    @Override
    public <T extends Configuration> T getCompositeConfiguration() {
        return null;
    }

    @Override
    public <T extends Configuration> T getCompositeConfigurationSubset(String prefix) {
        return null;
    }

    @Override
    public <T extends Configuration> T getConfiguration(String sourceId) {
        return null;
    }

    @Override
    public <T extends Configuration> T getConfigurationSubset(String prefix, String sourceId) {
        return null;
    }

    @Override
    public ConfigurationSourceResolver getConfigurationSourceResolver() {
        return null;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected void shutdown() {

    }

}
