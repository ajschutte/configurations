package com.mg.clients.configurations.agent.spring;

import com.mg.models.configurations.ConfigurationAgent;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.ResourceLoader;

/**
 * @author Andries on 1/25/18.
 */
@Configuration
public class SpringConfigurationAgentContext {

    private static ConfigurationAgent configurationAgent;

    @Qualifier("spring-config")
    @Bean
    public ConfigurationAgent configurationAgent(ConfigurableEnvironment env, ResourceLoader loader) {
        configurationAgent = new SpringConfigurationAgent(env, loader);
        return configurationAgent;
    }

    public static ConfigurationAgent getConfigurationAgent() {
        return configurationAgent;
    }

}
