package com.mg.clients.configurations.agent.spring;

import com.mg.models.configurations.ConfigurationAgent;
import com.mg.models.configurations.ConfigurationAgentLauncher;

/**
 * @author Andries on 2/1/18.
 */
public class SpringConfigurationAgentLauncher implements ConfigurationAgentLauncher {

    public static String PROVIDER_ID = "spring";

    @Override
    public ConfigurationAgent getConfigurationAgent() {
        return SpringConfigurationAgentContext.getConfigurationAgent();
    }

    @Override
    public String getProviderId() {
        return PROVIDER_ID;
    }

}
