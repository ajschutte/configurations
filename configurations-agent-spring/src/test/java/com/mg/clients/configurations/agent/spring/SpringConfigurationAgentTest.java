package com.mg.clients.configurations.agent.spring;

import com.mg.models.configurations.ConfigurationAgent;
import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

import static com.mg.models.configurations.BaseConfigurationAgent.CONFIG_BASE_DIR_SYSTEM_PROP;
import static com.mg.models.configurations.BaseConfigurationAgent.CONFIG_PROFILES_SYSTEM_PROP;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author Andries on 1/25/18.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {SpringConfigurationAgentContext.class})
public class SpringConfigurationAgentTest {

    private static final Logger logger = LoggerFactory.getLogger(SpringConfigurationAgentTest.class);

    @Qualifier("spring-config")
    @Autowired
    private ConfigurationAgent agent;

    @BeforeClass
    public static void init() {

        try {

            Path path = Paths.get(SpringConfigurationAgentTest.class.getClassLoader()
                    .getResource("config/test-fs.yml").toURI());

            StringBuilder data = new StringBuilder();
            Stream<String> lines = Files.lines(path);
            lines.forEach(line -> data.append(line).append("\n"));
            lines.close();

            assertTrue(StringUtils.isNotEmpty(data.toString()));

            String tmpDir = System.getProperty("java.io.tmpdir");

            logger.info("OS temporary directory: {}", tmpDir);

            if (!tmpDir.endsWith("/")) {
                tmpDir = tmpDir + "/";
            }

            Path tmpConfig = FileSystems.getDefault().getPath(tmpDir, "application-bar.yml");

            boolean result = Files.deleteIfExists(tmpConfig);

            logger.info("Deleted pre-existing temporary config? {}", result);

            Files.write(tmpConfig, data.toString().getBytes());

            System.setProperty(CONFIG_BASE_DIR_SYSTEM_PROP, tmpDir);
            System.setProperty(CONFIG_PROFILES_SYSTEM_PROP, "foo,bar");

        }
        catch (Exception exx) {

            logger.error("Error in init()..", exx);
            fail("Can't init()..");

        }

    }

    @Test
    public void createConfiguration_configurationAgentNotNull() {

        assertNotNull(SpringConfigurationAgentContext.getConfigurationAgent());

    }

    @Test
    public void createConfiguration_configurableEnvironmentNotNull() {

        ConfigurationAgent agent = ConfigurationAgent.getConfigurationAgent(SpringConfigurationAgentLauncher.PROVIDER_ID);

        assertNotNull(agent.getConfigurableEnvironment());

    }

    @Test
    public void createYamlConfiguration_validFileSystemProperties() {

        ConfigurableEnvironment env = (ConfigurableEnvironment)agent.getConfigurableEnvironment();

        assertNotNull(env);

        PropertySource source = env.getPropertySources().get("applicationConfigurationProperties");

        assertNotNull(source);
        assertTrue(source instanceof EnumerablePropertySource);

        EnumerablePropertySource enumSource = (EnumerablePropertySource)source;

        Arrays.asList(enumSource.getPropertyNames()).forEach(
                key -> {
                    logger.info("Property Name Found: {} with value: {}", key, env.getProperty(key));
                    assertTrue(Arrays.asList("Filesystem.RootPath", "Filesystem.WebPath").contains(key));
                }
        );

    }

}
