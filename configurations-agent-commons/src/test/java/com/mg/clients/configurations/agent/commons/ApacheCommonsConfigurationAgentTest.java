package com.mg.clients.configurations.agent.commons;

import com.mg.models.configurations.ConfigurationAgent;
import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.apache.commons.configuration2.YAMLConfiguration;
import org.apache.commons.configuration2.tree.ImmutableNode;
import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

import static com.mg.models.configurations.BaseConfigurationAgent.CONFIG_BASE_DIR_SYSTEM_PROP;
import static com.mg.models.configurations.BaseConfigurationAgent.CONFIG_PROFILES_SYSTEM_PROP;
import static org.junit.Assert.*;

/**
 * @author Andries on 1/25/18.
 */
public class ApacheCommonsConfigurationAgentTest {

    private static final Logger logger = LoggerFactory.getLogger(ApacheCommonsConfigurationAgentTest.class);

    @BeforeClass
    public static void init() {

        try {

            Path path = Paths.get(ApacheCommonsConfigurationAgentTest.class.getClassLoader()
                    .getResource("config/test-fs.yml").toURI());

            StringBuilder data = new StringBuilder();
            Stream<String> lines = Files.lines(path);
            lines.forEach(line -> data.append(line).append("\n"));
            lines.close();

            assertTrue(StringUtils.isNotEmpty(data.toString()));

            String tmpDir = System.getProperty("java.io.tmpdir");

            logger.info("OS temporary directory: {}", tmpDir);

            if (!tmpDir.endsWith("/")) {
                tmpDir = tmpDir + "/";
            }

            Path tmpConfig = FileSystems.getDefault().getPath(tmpDir, "application.yml");

            boolean result = Files.deleteIfExists(tmpConfig);

            logger.info("Deleted pre-existing temporary config? {}", result);

            Files.write(tmpConfig, data.toString().getBytes());

            //Stream <String> lines = Files.lines(path);
            //List <String> replaced = lines.map(line -> line.replaceAll("foo", "bar")).collect(Collectors.toList());
            //Files.write(path, replaced);
            //lines.close();

            System.setProperty(CONFIG_BASE_DIR_SYSTEM_PROP, tmpDir);
            System.setProperty(CONFIG_PROFILES_SYSTEM_PROP, "foo,bar");

        }
        catch (Exception exx) {

            logger.error("Error in init()..", exx);
            fail("Can't init()..");

        }

    }

    @Test
    public void createYamlConfiguration_validFileSystemProperties() {

        ConfigurationAgent agent = ConfigurationAgent.getConfigurationAgent(ApacheCommonsConfigurationAgentLauncher.PROVIDER_ID);

        assertNotNull(agent.getConfigurableEnvironment());

    }

    @Test
    public void getYamlProperty_topLevelHierarchyNotNull() {

        ConfigurationAgent agent = ConfigurationAgent.getConfigurationAgent(ApacheCommonsConfigurationAgentLauncher.PROVIDER_ID);

        YAMLConfiguration config = (YAMLConfiguration)agent.getConfigurableEnvironment();

        HierarchicalConfiguration<ImmutableNode> prop = config.configurationAt("Filesystem");

        assertNotNull(prop);

        logger.info("Display: {}", prop.getNodeModel().getInMemoryRepresentation());

        prop.getKeys().forEachRemaining(key -> {
            logger.info("Child key found in hierarchy: {}", key);
            assertTrue(Arrays.asList("RootPath", "WebPath").contains(key));
        });

    }

    @Test
    public void getYamlProperty_nestedLeafHierarchyAsExpected() {

        ConfigurationAgent agent = ConfigurationAgent.getConfigurationAgent(ApacheCommonsConfigurationAgentLauncher.PROVIDER_ID);

        YAMLConfiguration config = (YAMLConfiguration)agent.getConfigurableEnvironment();

        Object prop = config.getProperty("Filesystem.RootPath");

        logger.info("Found nested property: {}", prop);

        assertEquals("/from/filesystem", prop);

    }

}
