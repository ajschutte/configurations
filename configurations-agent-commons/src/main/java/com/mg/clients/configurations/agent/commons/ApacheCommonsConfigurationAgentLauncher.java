package com.mg.clients.configurations.agent.commons;

import com.mg.models.configurations.ConfigurationAgent;
import com.mg.models.configurations.ConfigurationAgentLauncher;

/**
 * @author Andries on 2/1/18.
 */
public class ApacheCommonsConfigurationAgentLauncher implements ConfigurationAgentLauncher {

    public static String PROVIDER_ID = "commons";

    @Override
    public ConfigurationAgent getConfigurationAgent() {
        return new ApacheCommonsConfigurationAgent();
    }

    @Override
    public String getProviderId() {
        return PROVIDER_ID;
    }

}
