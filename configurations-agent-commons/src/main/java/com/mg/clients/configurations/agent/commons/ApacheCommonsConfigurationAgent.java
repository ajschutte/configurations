package com.mg.clients.configurations.agent.commons;

import com.mg.models.configurations.BaseConfigurationAgent;
import com.mg.exceptions.core.ResourceConfigurationFailedException;
import com.mg.models.configurations.Configuration;
import com.mg.models.configurations.ConfigurationSourceResolver;
import org.apache.commons.configuration2.YAMLConfiguration;
import org.apache.commons.configuration2.builder.ReloadingFileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.reloading.PeriodicReloadingTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * @author Andries on 1/25/18.
 */
public class ApacheCommonsConfigurationAgent extends BaseConfigurationAgent {

    private static final Logger logger = LoggerFactory.getLogger(ApacheCommonsConfigurationAgent.class);

    private ReloadingFileBasedConfigurationBuilder<YAMLConfiguration> builder;

    private File propsFile;

    public ApacheCommonsConfigurationAgent() {

        String path = null;

        try  {

            propsFile = new File(System.getProperty(CONFIG_BASE_DIR_SYSTEM_PROP), "application.yml");
            String profiles = System.getProperty(CONFIG_PROFILES_SYSTEM_PROP);

            path = propsFile.getAbsolutePath();

            logger.debug("C'tor invoked, using base config path: {}", path);

            builder = new ReloadingFileBasedConfigurationBuilder<>(YAMLConfiguration.class)
                            .configure(new Parameters().fileBased()
                                    .setReloadingRefreshDelay(100L)
                                    .setFile(propsFile));

            PeriodicReloadingTrigger trigger = new PeriodicReloadingTrigger(builder.getReloadingController(),
                    null, 10, TimeUnit.SECONDS);
            trigger.start();

        }
        catch (Exception exx) {
            logger.error("Error in c'tor..", exx);
            throw new ResourceConfigurationFailedException("Could not create a configuration from base config path: " + path);
        }

    }

    @Override
    public Object getConfigurableEnvironment() {

        try {
            return builder.getConfiguration();
        }
        catch (ConfigurationException exx) {
            logger.error("Error in getConfigurableEnvironment()..", exx);
            throw new ResourceConfigurationFailedException(exx);
        }

    }

    @Override
    public <T extends Configuration> T getCompositeConfiguration() {
        return null;
    }

    @Override
    public <T extends Configuration> T getCompositeConfigurationSubset(String prefix) {
        return null;
    }

    @Override
    public <T extends Configuration> T getConfiguration(String sourceId) {
        return null;
    }

    @Override
    public <T extends Configuration> T getConfigurationSubset(String prefix, String sourceId) {
        return null;
    }

    @Override
    public ConfigurationSourceResolver getConfigurationSourceResolver() {
        return null;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected void shutdown() {

    }

}
